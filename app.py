from flask import Flask, render_template,request,flash
from tools import str_to_newdata,plot
import xgboost as xgb
app = Flask(__name__)
app.secret_key = "super secret key"
@app.route("/",methods = ['GET','POST'])
def hello_world():
    # return "<p>Hello, World!</p>"
    request_type_str = request.method
    if request_type_str == 'GET':
        path = "static/baseplot.html"
        
    else:
        # print(request.form)
        
        if 'new_data' in request.form:
            text = request.form['new_data']
        else:
            text = ",".join([v for k,v in request.form.items()])
        path = "static/plusplot.html"
        new_data = str_to_newdata(text)
        if type(new_data) == str:
            flash(new_data)
            return render_template('index.html',href = "static/baseplot.html")

        model = xgb.XGBClassifier(objective="binary:logistic")
        model.load_model("xgb.txt")
        plot(model,new_data,path)
    
    return render_template('index.html',href = path)