import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def plot(model,new_data,output_file):
    df = pd.read_csv('data.csv')
    fig = make_subplots(
    rows=2,cols=2)
    # 1
    target = df['Potability']
    fig.add_trace(
        go.Box(y=df[target==0]['ph'],name="Undrinkable",marker_color = 'indianred'),row=1,col=1
        )
    fig.add_trace(
        go.Box(y=df[target==1]['ph'],name="Drinkable",marker_color = 'lightseagreen'),row=1,col=1
        )
    # 2
    fig.add_trace(
        go.Box(y=df[target==0]['Hardness'],name="Undrinkable",marker_color = 'indianred',showlegend=False),row=1,col=2
        )
    fig.add_trace(
        go.Box(y=df[target==1]['Hardness'],name="Drinkable",marker_color = 'lightseagreen',showlegend=False),row=1,col=2
        )
    # 3
    fig.add_trace(
        go.Box(y=df[target==0]['Solids'],name="Undrinkable",marker_color = 'indianred',showlegend=False),row=2,col=1
        )
    fig.add_trace(
        go.Box(y=df[target==1]['Solids'],name="Drinkable",marker_color = 'lightseagreen',showlegend=False),row=2,col=1
        )
    # 4
    fig.add_trace(
        go.Box(y=df[target==0]['Sulfate'],name="Undrinkable",marker_color = 'indianred',showlegend=False),row=2,col=2
        )
    fig.add_trace(
        go.Box(y=df[target==1]['Sulfate'],name="Drinkable",marker_color = 'lightseagreen',showlegend=False),row=2,col=2
        )

    # add new data
    new_y = model.predict(new_data)[0]
    nf_1 = new_data[0][0]
    nf_2 = new_data[0][1]
    nf_3 = new_data[0][2]
    nf_4 = new_data[0][4]
    if new_y == 1:
        prediction = 'Drinkable'
    else:
        prediction = 'Undrinkable' 

    fig.add_trace(
        go.Scatter(
            x=[prediction],
            y=[nf_1],mode="markers",
            marker = dict(
                color = "#C619F5",
                size = 15
                ),
                line = dict(color="#C619F5",width=1),
                name="new_data"
        ),row=1,col=1
    )
    fig.add_trace(
        go.Scatter(
            x=[prediction],
            y=[nf_2],mode="markers",
            marker = dict(
                color = "#C619F5",
                size = 15
                ),
                line = dict(color="#C619F5",width=1),
                showlegend=False
        ),row=1,col=2
    )
    fig.add_trace(
        go.Scatter(
            x=[prediction],
            y=[nf_3],mode="markers",
            marker = dict(
                color = "#C619F5",
                size = 15
                ),
                line = dict(color="#C619F5",width=1),
                showlegend=False
        ),row=2,col=1
    )    
    fig.add_trace(
        go.Scatter(
            x=[prediction],
            y=[nf_4],mode="markers",
            marker = dict(
                color = "#C619F5",
                size = 15
                ),
                line = dict(color="#C619F5",width=1),
                showlegend=False
        ),row=2,col=2
    )

    # xaxis
    fig.update_xaxes(title_text= "ph Value",row=1,col=1)
    fig.update_xaxes(title_text= "Hardness",row=1,col=2)
    fig.update_xaxes(title_text= "Solids",row=2,col=1)
    fig.update_xaxes(title_text= "Sulfate",row=2,col=2)
    # yaxis
    fig.update_layout(height=1000,width=1400,title_text="Potability Detection")
    fig.write_html(output_file)


def str_to_newdata(text:str):
    try:
        l = text.split(',')
        l = [float(i) for i in l]
        
        if len(l) != 9:
            return "Don't have enough variable!"
        new_data = np.array(l).reshape(1,-1)
    except Exception:
        return "Format is not correct!"
    
    return new_data
    